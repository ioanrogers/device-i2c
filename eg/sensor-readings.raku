#!/usr/bin/env raku

sub MAIN(
    Bool  :$debug,     #= Enable debug output
    Str:D :$i2c-path,  #= Path to i2c bus device
    Str:D :$chip,      #= Name of the sensor
    Int   :$dev-addr,  #= i2c address of the sensor
) {

    my $sensor-module = 'Device::I2C::Sensor::' ~ $chip.uc;
    try require ::($sensor-module);
    if ::($sensor-module) ~~ Failure {
        die "Failed to load $sensor-module: $!";
    }

    my %args =
      debug => $debug,
      i2c-bus-device-path => $i2c-path;

    %args<i2c-device-address> = $dev-addr if $dev-addr;

    my $sensor = ::($sensor-module).new(|%args);
    $sensor.reset if $sensor.^can('reset');

    my $senses = $sensor.get-sensor-features;

    loop {
        my %reading;
        for $senses.values -> $sense {
            %reading{$sense} = $sensor."get-$sense"();
        }

        print DateTime.now;
        printf " %s=%.2f", $_, %reading{$_} for $senses.values;
        print "\n";

        sleep 30;
    }
}
