use v6;

unit role Device::I2C:ver<0.0.1>:auth<gitlab:ioanrogers>[$address];

use Device::I2C::IO;

has Bool $.debug = False;
has Str $.i2c-bus-device-path is required;
has Int $.i2c-device-address = $address;

has $!i2c-io = Device::I2C::IO.new(
    bus-path => $!i2c-bus-device-path,
    device-address => $!i2c-device-address,
);

method get-sensor-features() returns List {
    my $senses = map { .^name.split('::')[*- 1].lc },
        self.^roles(:!transitive).grep: { .^name.match(/^Device\:\:I2C\:\:SensorType/) };
    return $senses.List;

}
