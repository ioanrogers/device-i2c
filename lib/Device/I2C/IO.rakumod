use v6;

unit class Device::I2C::IO;

use NativeCall;

subset RegisterAddress of Int where {  $_ >= 0 && $_ <= 256 };

constant I2C_SLAVE = 0x0703;

has Str $.bus-path is required;
has Int $.device-address is required;

sub ioctl(int32, ulong, int32 --> int32) is native { * }

method write-byte(RegisterAddress:D $register-address) {

    my $bus-fh = $!bus-path.IO.open(:update);

    if (!$bus-fh) {
        $bus-fh.die;
    }

    my $rc = ioctl($bus-fh.native-descriptor, I2C_SLAVE, $!device-address);
    if ($rc < 0) {
        # TODO check errno
        die 'Failed to address device on i2c bus';
    }

    my $command_buf = buf8.new($register-address);

    $bus-fh.write($command_buf) orelse .die;
    $bus-fh.close;

    return;
}

method write-bytes(Buf:D $bytes) {

    my $bus-fh = $!bus-path.IO.open(:update);

    if (!$bus-fh) {
        $bus-fh.die;
    }

    my $rc = ioctl($bus-fh.native-descriptor, I2C_SLAVE, $!device-address);
    if ($rc < 0) {
        # TODO check errno
        die 'Failed to address device on i2c bus';
    }

    $bus-fh.write($bytes) orelse .die;
    $bus-fh.close;

    return;
}

method read-bytes(Int $num-bytes = 1 --> Buf) {

    my $bus-fh = $!bus-path.IO.open(:update);

    if (!$bus-fh) {
        $bus-fh.die;
    }

    my $rc = ioctl($bus-fh.native-descriptor, I2C_SLAVE, $!device-address);
    if ($rc < 0) {
        # TODO check errno
        die 'Failed to address device on i2c bus';
    }

    my $buf = $bus-fh.read($num-bytes);
    $bus-fh.close;
    return $buf;
}

=begin pod

=head1 NAME

Device::I2C - blah blah blah

=head1 SYNOPSIS

=begin code :lang<raku>

use Device::I2C;

my $i2c-bus = Device::I2C.new(
  bus-path => /dev/i2c-3,
  device-address => 0x40,
);

=end code

=head1 DESCRIPTION

Device::I2C is ...

=head1 AUTHOR

Ioan Rogers <ioan@rgrs.ca>

=head1 COPYRIGHT AND LICENSE

Copyright 2021 Ioan Rogers

This library is free software; you can redistribute it and/or modify it under the Artistic License 2.0.

=end pod
