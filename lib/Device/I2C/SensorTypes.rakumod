use v6;

role Device::I2C::SensorType::Temperature {
    method get-temperature returns Rat { ... }
}

role Device::I2C::SensorType::Humidity {
    method get-humidity returns Rat { ... }
}
