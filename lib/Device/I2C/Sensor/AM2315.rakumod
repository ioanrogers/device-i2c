use v6;

use Device::I2C;
use Device::I2C::SensorTypes;

unit class Device::I2C::Sensor::AM2315
        does Device::I2C[0x5C]
        does Device::I2C::SensorType::Temperature
        does Device::I2C::SensorType::Humidity;

constant CMD_READ = 0x03;

has Instant $.last-reading-time is rw;
has Rat %.reading is rw;

method wakeup {
    note "Trying to wake up am2315 at $.i2c-device-address.fmt('0x%x')" if $.debug;
    for 1 .. 10 {
        try {
            $!i2c-io.write-byte(0);
            CATCH {
                when X::AdHoc {
                    .Str.say;
                    # Failed to write bytes to filehandle: No such device or address
                    sleep 0.25;
                }
            }
            last;
        }
    }
    # wait 10ms for it to be ready
    sleep 0.01;
}

# pretty much straight from the data sheet
# don't know if there's a more idiomatic way
method !crc(Buf:D $bytes) {
    my $crc = 0xffff;

    # the last 2 bytes are the crc from the device
    for 0 .. $bytes.elems - 3 -> $i {
        $crc +^= $bytes[$i];

        for 0 .. 7 {
            if $crc +& 0x0001 {
                $crc +>= 1;
                $crc +^= 0xA001;
            } else {
                $crc +>= 1;
            }
        }
    }
    return $crc;
}

method !parse-temperature(Int:D $high, Int:D $low --> Rat) {
    # the first bit of the high byte is the sign
    my $temp = ($high +< 8 +| $low) +& 0x7fff;

    if ($high +& 0x80) {
        $temp *= -1;
    }
    return $temp / 10;
}

method !parse-humidity(Int:D $high, Int:D $low --> Rat) {
    return ($high +< 8 +| $low) / 10;
}

method !read() {
    self.wakeup;

    my $cmd = Buf.new(CMD_READ, 0, 4);
    if $.debug {
        for $cmd.list -> $byte {
            $*ERR.printf("writing: %#.2x %#.8b\n", $byte, $byte);
        }
    }
    $!i2c-io.write-bytes($cmd);
    # datasheet says wait at least 1.5ms
    sleep 0.0015;

    my $bytes = $!i2c-io.read-bytes(8);

    if $.debug {
        for $bytes.list -> $b {
            $*ERR.printf("read: %#.2x %#.8b\n", $b, $b);
        }
    }

    # bytes[1] should be the same length we asked for
    if ($bytes[0] != CMD_READ or $bytes[1] != 4) {
        die "Incorrect bytes read from i2c: $bytes[0] $bytes[1]"
    }

    self.last-reading-time = now;

    # temp and humidity are (high, low) but crc is (low, high)
    my $calc-crc = self!crc($bytes);
    my $got-crc = ($bytes[7] +< 8) +| $bytes[6];

    if $.debug {
        $*ERR.printf("got crc: %#.2x %#.8b\n", $got-crc, $got-crc);
        $*ERR.printf("calc crc: %#.2x %#.8b\n", $calc-crc, $calc-crc);
    }

    if $got-crc != $calc-crc {
        die "CRC check failed!";
    }

    self.reading<humidity> = self!parse-humidity: $bytes[2], $bytes[3];
    self.reading<temperature> = self!parse-temperature: $bytes[4], $bytes[5];

    return;
}

method get-temperature(--> Rat) {
    if (!$.reading<temperature> or now - self.last-reading-time > 10) {
        self!read;
    }
    return $.reading<temperature>;
}

method get-humidity(--> Rat) {
    if (!$.reading<humidity> or now - self.last-reading-time > 10) {
        self!read;
    }

    return $.reading<humidity>;
}
