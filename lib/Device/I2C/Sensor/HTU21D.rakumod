use v6;

use Device::I2C;
use Device::I2C::SensorTypes;

unit class Device::I2C::Sensor::HTU21D
    does Device::I2C[0x40]
    does Device::I2C::SensorType::Temperature
    does Device::I2C::SensorType::Humidity;

constant CMD_GET_TEMP_HOLD       = 0xE3; # F3 for non-blocking
constant CMD_GET_HUMI_HOLD       = 0xE5; # F5 for non-blocking
constant CMD_SOFT_RESET          = 0xFE;

method reset() {
    note "Resetting device" if $.debug;
    $!i2c-io.write-byte(CMD_SOFT_RESET);

    # give it some time to finish resetting
    sleep 0.015;
    return;
}

method !get-raw-reading(Int:D $register-address --> Int) {
    $!i2c-io.write-byte($register-address);
    # XXX There is supposed to be a little time before the measurement is made
    # for the HOLD methods of reading, but it varies for measurement type and
    # resolution. 50ms is the longest possibility.
    sleep 0.05;

    my $bytes = $!i2c-io.read-bytes(3);

    if $.debug {
        $*ERR.printf("high: %#.2x %#.8b\n", $bytes[0], $bytes[0]);
        $*ERR.printf("low:  %#.2x %#.8b\n", $bytes[1], $bytes[1]);
	    $*ERR.printf("crc:  %#.2x %#.8b\n", $bytes[2], $bytes[2]);
    }

    my $raw-reading = ($bytes[0] +< 8 +| $bytes[1]);

    # clear the last two bits which are flags
    $raw-reading +&= 0xFFFC;
    $*ERR.printf("raw: %#.4x %#.16b\n", $raw-reading, $raw-reading) if $.debug;

    return $raw-reading;
}

method get-temperature {
    my $raw-reading = self!get-raw-reading(CMD_GET_TEMP_HOLD);

    # magic numbers from the datasheet
    return -46.85 + 175.72 * ($raw-reading / 2 ** 16);
}

method get-humidity(--> Rat) {
    my $raw-reading = self!get-raw-reading(CMD_GET_HUMI_HOLD);

    # magic numbers from the datasheet
    my $actual-rh = -6 + 125 * ($raw-reading / 2 ** 16);

    # between 0 and 80 degrees
    my $temp-coefficient = -0.15;
    my $compensated-rh = $actual-rh + ( 25 - self.get-temperature ) * $temp-coefficient;

    return $compensated-rh;
}
