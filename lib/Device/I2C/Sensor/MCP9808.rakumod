use v6;

use Device::I2C;
use Device::I2C::SensorTypes;

unit class Device::I2C::Sensor::MCP9808
    does Device::I2C[0x18]
    does Device::I2C::SensorType::Temperature;

constant MANUFACTURER_ID = 0x54;
constant DEVICE_ID       = 0x04;

constant CMD_GET_TEMPERATURE     = 0x05;
constant CMD_GET_MANUFACTURER_ID = 0x06;
constant CMD_GET_DEVICE_ID       = 0x07;

submethod TWEAK {
    #self.assert-manufacturer;
    #self.assert-device;
}

method assert-manufacturer {
    $!i2c-io.write-byte(CMD_GET_MANUFACTURER_ID);
    my $bytes = $!i2c-io.read-bytes(2);

    if $!debug {
        $*ERR.printf("manufacturer low:  %#.2x %#.8b\n",
          $bytes[0], $bytes[0]);
        $*ERR.printf("manufacturer high: %#.2x %#.8b\n",
          $bytes[1], $bytes[1]);
    }

    unless ($bytes[1] == MANUFACTURER_ID) {
        die sprintf("Invalid manufacturer ID %#.2x", $bytes);
    }

    return;
}

method assert-device {
    $!i2c-io.write-byte(CMD_GET_DEVICE_ID);
    my $bytes = $!i2c-io.read-bytes(2);

    if $!debug {
        $*ERR.printf("device id:       %#.2x %#.8b\n",
          $bytes[0], $bytes[0]);
        $*ERR.printf("device revision: %#.2x %#.8b\n",
          $bytes[1], $bytes[1]);
    }

    unless ($bytes[0] == DEVICE_ID) {
        die sprintf("Invalid device ID %#.2x", $bytes[0]);
    }

    return;
}

method get-temperature(--> Rat) {
    $!i2c-io.write-byte(CMD_GET_TEMPERATURE);
    my $bytes = $!i2c-io.read-bytes(2);

    if $!debug {
        $*ERR.printf("temp high: %#.2x %#.8b\n", $bytes[0], $bytes[0]);
        $*ERR.printf("temp low:  %#.2x %#.8b\n", $bytes[1], $bytes[1]);
    }

    # Clear flag bits
    $bytes[0] +&= 0x1f;
    $*ERR.printf("temp high: %#.2x %#.8b\n", $bytes[0], $bytes[0]) if $!debug;

    if (($bytes[0] +& 0x10) == 0x10) {

        # temp < 0℃
        $bytes[0] = $bytes[0] & 0x0F;
        # clear SIGN
        return 256 - ($bytes[0] * 16 + $bytes[1] / 16);
    }
    return $bytes[0] * 16 + $bytes[1] / 16;
}
